# Installation

Install Ruby -> [Ruby](https://www.ruby-lang.org/en/documentation/installation/)

Run:

```
    gem install bundler
```

Install Appium -> [Appium](http://appium.io/docs/en/about-appium/getting-started/)

Install dependecies running:
```
    bundle install
```


# How to run

```
    cucumber -r support/env.rb
```